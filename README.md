# README #

# Work In Progress #
This project might lack documentation and is not finished. Code standards might not be up to par with finished projects at the time being.

#
Author: Pablo Gonz�lez Mart�nez
#
pablogonzmartinez@gmail.com

Visual Studio 2015, C++

This project showcases code base for a C++ engine, to be used as a library.
It currently uses OpenGL for its rendering module, and is set to include Bullet for physics implementation.

# Google Docs doc #
https://docs.google.com/document/d/1iXRRT5uqYSSOHN8_VRFeq14LbmHY7ibcY6Qun9rrNYw/edit?usp=sharing

# After-download guide: #
It should be opened in Visual Studio via the project file found in engine -> projects -> PGZMZ_Engine

The testing is temporarily being done directly in the main.cpp of the project.

# Errors of the current implementation #
My obj loader is not working correctly with the VBO indexer and models are not rendered correctly with OpenGL.