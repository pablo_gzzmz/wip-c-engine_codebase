
#include "Camera_Component.hpp"

namespace rendering {
	typedef Render_Module::Camera_Component Camera_Component;

	Camera_Component * Camera_Component::main_camera = nullptr;
	vector<Camera_Component *> Camera_Component::camera_cache;

	bool Camera_Component::initialize ()  { 
		if (main_camera == nullptr) {
			make_main();
		}

		camera_cache.push_back(this);
		return true;
	}
}