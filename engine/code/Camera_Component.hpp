
#ifndef ENGINE_CAMERA_COMPONENT_HEADER
#define ENGINE_CAMERA_COMPONENT_HEADER

#include <vector>
#include "Render_Module.hpp"
#include "Color.hpp"

namespace rendering {
	using std::vector;
	typedef engine_core::Color  Color;

	class Render_Module::Camera_Component : public Component {
	private:
		enum {
			PERSPECTIVE,
			ORTHOGRAPHIC
		} projection_mode;

		static Camera_Component * main_camera;
		static vector<Camera_Component *> camera_cache;

		float	fov;
		float	near_plane;
		float   far_plane;
		float	aspect_ratio;
		Mat4x4	projection_matrix;

		Color view_color;

	public:
		Camera_Component(Entity * parent_entity) : Component(parent_entity) {}

		Camera_Component(Entity * parent_entity, float fov, float nearp, float farp, float aspectrat) : Component(parent_entity){
			set(fov, nearp, farp, aspectrat);
		}


	public:
		bool	initialize	  () override;

		bool	duplicable	  () override { return false; }

		string	component_name() override { return "camera"; }

		bool	parse_property(const string & name, const string & value) override { return true; }

	
	public:
		       void			      make_main() { main_camera = this; }
		static Camera_Component *  get_main() { return main_camera; }

	public:
		void set(float _fov, float _near_plane, float _far_plane, float _aspect_ratio) {
			fov			 = _fov;
			near_plane	 = _near_plane;
			far_plane	 = _far_plane;
			aspect_ratio = _aspect_ratio;
			
			refresh_matrix();
		}

		void set_fov(float _fov){
			fov = _fov;

			refresh_matrix();
		}

		void set_near(float _near_plane){
			near_plane = _near_plane;

			refresh_matrix();
		}

		void set_far(float _far_plane){
			far_plane = _far_plane;

			refresh_matrix();
		}

		void set_aspect_ratio(float _aspect_ratio){
			aspect_ratio = _aspect_ratio;

			refresh_matrix();
		}

		void set_view_color(Color c) {
			view_color = c;
		}

		void set_view_color(int r, int g, int b) {
			view_color.set(r, g, b);
		}

		glm::mat4x4 get_projection_matrix() { return projection_matrix; }

	public:
		const float view_color_r() { return view_color.normalized_r(); }
		const float view_color_g() { return view_color.normalized_g(); }
		const float view_color_b() { return view_color.normalized_b(); }

	public:
		static bool can_render() { return !camera_cache.empty(); }

	private:
		void refresh_matrix() {
			projection_matrix = math::perspective(fov, near_plane, far_plane, aspect_ratio);
		}

	};
}

#endif
