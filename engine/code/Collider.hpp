
#ifndef ENGINE_COLLIDER_HEADER
#define ENGINE_COLLIDER_HEADER

#include "Physics_Module.hpp"

namespace physics {

	class Physics_Module::Collider {
	protected:
		shared_ptr<btCollisionShape> shape;

	public:
		btCollisionShape * get_shape() const { return shape.get(); }
	
	};
}

#endif
