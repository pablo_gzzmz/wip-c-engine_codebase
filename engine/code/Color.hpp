
#ifndef ENGINE_COLOR_STRUCT_HEADER
#define ENGINE_COLOR_STRUCT_HEADER

#include <cstdint>

namespace engine_core {

	struct Color {
		union {
			struct {
				uint8_t r;
				uint8_t g;
				uint8_t b;
				uint8_t a;
			}
			component;

			uint32_t value;
		}
		data;

		void set(int r, int g, int b, int a = 255) {
			data.component.r = r;
			data.component.g = g;
			data.component.b = b;
			data.component.a = a;
		}

		float normalized_r() { return data.component.r * 0.0039215686f; }
		float normalized_g() { return data.component.g * 0.0039215686f; }
		float normalized_b() { return data.component.b * 0.0039215686f; }

		Color & operator = (const int & value) {
			data.value = uint32_t(value);
			return (*this);
		}
	};
}

#endif
