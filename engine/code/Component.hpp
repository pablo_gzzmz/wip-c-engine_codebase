
#ifndef ENGINE_COMPONENT_HEADER
#define ENGINE_COMPONENT_HEADER

#include <string>

namespace engine_core {
	using std::string;

	class Entity;

	class Component {
	protected:
		Entity * entity;

		bool enabled;

	public:
		Component(Entity * entity) : entity(entity) {
			enabled = true;
		}

	public:
		virtual bool	initialize	   () = 0;

		virtual bool	duplicable	   () = 0;

		virtual string	component_name () = 0;

		virtual bool	parse_property (const string & name, const string & value) = 0;
	
	public:
		void set_enabled(bool state)	   { enabled = state; }
		bool is_enabled ()			 const { return enabled;  }

		Entity * get_entity() { return entity; }
	};
}

#endif