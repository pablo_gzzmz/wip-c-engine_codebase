
#include "Entity.hpp"
#include "Transform_Component.hpp"
#include "Scene.hpp"

namespace engine_core {
	Entity::Entity(string _name, Scene * _parent_scene, bool add_to_scene) : name(_name) {
		transform.reset(new Transform_Component(this));

		components.insert(std::make_pair(transform->component_name(), transform));

		transform->initialize();

		parent_scene = _parent_scene;

		if (add_to_scene) {
			parent_scene->add_entity(this);
		}
	}

	void Entity::hierarchy_update(Transform_Component * parent) {
		update();
		transform->update(parent);
		for (auto it = children.begin(); it != children.end(); ++it) {
			it->second->hierarchy_update(get_transform());
		}
	}
}