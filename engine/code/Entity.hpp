
#ifndef ENGINE_ENTITY_HEADER
#define ENGINE_ENTITY_HEADER

#include <unordered_map>
#include <memory>
#include <string>
#include "Messaging_Bus.hpp"

namespace engine_core {
	using std::unordered_multimap;
	using std::shared_ptr;
	using std::string;

	class Scene;
	class Transform_Component;
	class Component;

	class Entity {
	protected:
		shared_ptr<Transform_Component> transform;

		unordered_multimap<string, Entity *> children;
		unordered_multimap<string, shared_ptr<Component>> components;

		string name;

		Scene * parent_scene;

		bool entity_static;

	public:
		Entity(string _name, Scene * _parent_scene, bool add_to_scene = true);

	public:
		void hierarchy_update(Transform_Component * parent);

	protected:
		virtual void update() {}

	public:
		void add_child(Entity * entity) {
			children.insert(std::make_pair(entity->name, entity));
		}

		Entity * get_child (const string & name) { 
			return children.count(name) > 0 ? children.find(name)->second : nullptr; 
		}

		template<class T>
		T * add_component();

		Component * get_component(const string & type) { 
			return components.count(type) > 0 ? components.find(type)->second.get() : nullptr; 
		}

		Transform_Component * get_transform() { return transform.get(); }

	public:
		const Scene * get_scene() const { return parent_scene; }

		bool is_entity_static	  ()				{ return entity_static; }

		void set_static_as_entity (bool set_static) { entity_static = set_static; }

		void switch_entity_static ()				{ entity_static = !entity_static; }
	};

	template<class T>
	T * Entity::add_component() {
		shared_ptr<T> component(new T(this));
		string type = component->component_name();

		if (components.count(type) != 0) {
			if(!component->duplicable())
				return nullptr;
		}

		components.insert(std::make_pair(type, component));
		component->initialize();
		return component.get();
	}
}

#endif
