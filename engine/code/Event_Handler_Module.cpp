
#include "Event_Handler_Module.hpp"
#include "Kernel.hpp"
#include "Scene.hpp"
#include "Messaging_Bus.hpp"

namespace engine_core {
	Event_Handler_Module::Event_Handler_Task::Event_Handler_Task(Event_Handler_Module * _module) : module(_module){
		
	}

	bool Event_Handler_Module::Event_Handler_Task::step (float time) {
		Messaging_Bus * bus = module->get_parent_scene()->get_messaging_bus();

		// Default input messages
		Message key_down    ("key_down"	   );
		Message key_up	    ("key_up"	   );
		Message mouse_motion("mouse_motion");

		while (SDL_PollEvent(&sdl_event)) {
			switch (sdl_event.type) {

			case SDL_KEYDOWN:
				key_down.add_parameter(SDL_GetKeyName(sdl_event.key.keysym.sym), 0);
				bus->send(key_down);
				break;

			case SDL_KEYUP:
				key_up.add_parameter(SDL_GetKeyName(sdl_event.key.keysym.sym), 0);
				bus->send(key_up);
				break;

			case SDL_MOUSEMOTION:
				mouse_motion.add_parameter("x_motion", sdl_event.motion.x);
				mouse_motion.add_parameter("y_motion", sdl_event.motion.y);
				break;

			case SDL_QUIT:
				kernel->stop();
				break;

			}
		}

		return true;
	}
}