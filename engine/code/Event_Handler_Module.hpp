
/*
	Author: Pablo Gonz�lez Mart�nez

	Event_Handler_Module:
		- Handles event poll.
		- Dependency on SDL.
*/

#ifndef ENGINE_EVENT_HANDLER_MODULE_HEADER
#define ENGINE_EVENT_HANDLER_MODULE_HEADER

#include "SDL.h"
#include "Task.hpp"
#include "Module.hpp"
#include <memory>

namespace engine_core {
	using std::shared_ptr;

	class Event_Handler_Module : public Module {

		class Event_Handler_Task : public Task {
		private:
			Event_Handler_Module * module;

			SDL_Event sdl_event;

		public:
			Event_Handler_Task(Event_Handler_Module * module);

		public:
			int priority	() const	 override { return 15; }

			bool initialize ()			 override { return true; }

			bool step		(float time) override;

			bool finalize	()			 override { return true; }
		};

	private:
		Event_Handler_Task event_handler_task;

	public:
		Event_Handler_Module(string name, Scene * _parent_scene) 
			: event_handler_task(this), Module(name, _parent_scene, &event_handler_task) {}
	};
}

#endif
