
#ifndef ENGINE_INPUT_MODULE_HEADER
#define ENGINE_INPUT_MODULE_HEADER

#include <string>
#include <vector>
#include <map>
#include <memory>
#include "Module.hpp"

namespace engine_core {
	using std::string;
	using std::vector;
	using std::map;
	using std::shared_ptr;

	class Input_Module : public Module{

		class Input_Task : public Task {
		private:
			Input_Module  *  input_module;

			vector<string> input_triggers;

			int current_count;

		public:
			Input_Task(Input_Module * module) { input_module = module; }

		public:
			int  priority	() const	 override { return 14;   }

			bool initialize ()			 override { return true; }

			bool step		(float time) override;

			bool finalize	()			 override { return true; }

		public:
			void recieve_input(const string & input) {
				input_triggers.insert(input_triggers.begin() + current_count, input);
				current_count++;
			}
		};

	private:
		Input_Task input_task;

	public:
		Input_Module(string name, Scene * _parent_scene) : input_task(this), Module(name, _parent_scene, &input_task) {}

	};
}

#endif