
#include <cstdint>
#include <string>
#include <fstream>

namespace engine {
	using std::string;
	using std::ifstream;
	typedef uint8_t byte;

	class Input_Stream {
	public:
		virtual size_t read(byte * buffer, size_t amount) = 0;

		virtual bool eos () = 0;
		virtual bool good() = 0;
		virtual bool fail() = 0;
	};

	class File_Input_Stream : public Input_Stream {
	private:
		ifstream stream;

	public:
		File_Input_Stream(const string & path) : stream(path, ifstream::binary){}

		virtual size_t read(byte * buffer, size_t amount) override { return stream.read((char *)buffer, amount); }

		virtual bool eos() { stream.eof(); }
	};

	class Xor_Deobfuscate_Input_Stream : public Input_Stream{
		Input_Stream & source;
		byte key;

	public:
		Xor_Deobfuscate_Input_Stream(Input_Stream & _source, byte & _key) : source(_source), key(_key) {}

		virtual size_t read(byte * buffer, size_t amount) override { 
			size_t total_read = source.read(buffer, amount);

			for(size_t i = 0; i < total_read; i++){
				buffer[i] ^= key;
			}
		}

		virtual bool eos() { return source.eos(); }
	};
}

/*
 File_Input_Stream file_reader("data.xml");
 Xor_Deobfuscate_Input_Stream deobfuscator(file_reader, 0xAA);

vector<byte> data;
 byte buffer[4096];


	while(!deobfuscator.eos()){
		size_t total_read = deobfuscator.read(buffer, 4096);

		data.reserve(data.size() + total_read);

		for(size_t i = 0; i < total_read; ++i){
			data.push_back (buffer[i]);
		}
	}
*/