
#include "Kernel.hpp"
#include "Task.hpp"
#include "Timer.hpp"

namespace engine_core {
	void Kernel::add_task(Task * task) { 
		task->set_kernel(this); 
		task_set.insert(task); 
	}

	void Kernel::execute() {
		kernel_state = EXECUTE;
		exit		 = false;

		Timer timer;
	    delta_time = 0.016f;


		// INITIALIZATION
		for (auto task : task_set) {
			// Loading screen...
			task->initialize();
		}


		// EXECUTION
		while (!exit) {
			timer.initialize();

			for (auto task : task_set) {
				if (exit) break;

				switch (kernel_state) {
				case WAIT:
					while (kernel_state == WAIT) {

					}

				case EXECUTE:
					task->step(delta_time);
				}
			}

			delta_time = timer.elapsed_seconds();
		}


		// FINALIZATION
		for (auto task : task_set) {
			task->finalize();
		}
	}
}