
#ifndef ENGINE_KERNEL_HEADER
#define ENGINE_KERNEL_HEADER

#include <set>

namespace engine_core {
	using std::multiset;

	class Task;
	class Timer;

	class Kernel {
	private:
		enum Kernel_State {
			EXECUTE,
			WAIT
		} kernel_state;

		bool exit;

		multiset<Task *> task_set;

		float delta_time;

	public:
		void add_task(Task * task);

		void execute ();

		void stop	 () { exit = true;			 }

		void pause	 () { kernel_state = WAIT;	 }

		void resume	 () { kernel_state = EXECUTE; }

		float get_delta_time() { return delta_time; }
	};
}

#endif
