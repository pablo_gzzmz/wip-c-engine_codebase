
#include "Material.hpp"
#include "Color.hpp"

extern "C" {
    #include <targa.h>
}

namespace rendering {
	typedef engine_core::Color Color;
	typedef Render_Module::Material Material;
	
	auto_ptr<Texture2D> Material::load_texture(const char * texture_file_path) {
		std::auto_ptr<Texture2D> texture;
		tga_image              loaded_image;

		if (tga_read(&loaded_image, texture_file_path) == TGA_NOERR) {
			
			texture.reset(new Texture2D(loaded_image.width, loaded_image.height));

			// Pixel format to RGBA8888
			tga_convert_depth(&loaded_image, texture->bits_per_color());
			tga_swap_red_blue(&loaded_image);

			// Pixel from image buffer to texture buffer
			Color * loaded_image_pixels = reinterpret_cast<Color *>(loaded_image.image_data);
			Color * loaded_image_pixels_end = loaded_image_pixels + loaded_image.width * loaded_image.height;
			Color * image_buffer_pixels = texture->colors();

			while (loaded_image_pixels <  loaded_image_pixels_end) {
				*image_buffer_pixels++ = *loaded_image_pixels++;
			}

			tga_free_buffers(&loaded_image);
		}

		return (texture);
	}
}