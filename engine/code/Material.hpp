
#ifndef ENGINE_MATERIAL_HEADER
#define ENGINE_MATERIAL_HEADER

#include "Shader_Program.hpp"
#include "Texture.hpp"
#include "Render_Module.hpp"

namespace rendering {
	using std::auto_ptr;

	class Render_Module::Material {

		Shader_Program * shader;

		bool   textured;
		GLuint texture_id;

	public:
		Material(Shader_Program * _shader) {
			shader = _shader;
		}

		~Material() {
			if (textured) glDeleteTextures(1, &texture_id);
		}

	public:
		const bool is_textured     () { return textured; }

		const GLuint get_texture_id() { return texture_id; }

		auto_ptr<Texture2D> load_texture(const char * texture_file_path);

		Shader_Program * get_shader() { return shader; }

		void use_shader() { if (shader->is_usable()) shader->use(); }
	};
}

#endif