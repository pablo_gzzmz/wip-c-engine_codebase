
#ifndef ENGINE_MATH_HEADER
#define ENGINE_MATH_HEADER

#include "glm/glm.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/matrix_decompose.hpp"
#include "glm/gtc/matrix_transform.hpp"

namespace engine_core {

	typedef glm::mat4x4		Mat4x4;
	typedef glm::mat3x3     Mat3x3;
	typedef glm::mat2x2		Mat2x2;

	typedef glm::vec2		Vector2;
	typedef glm::vec3		Vector3;
	typedef glm::vec4		Vector4;

	typedef glm::quat		Quaternion;

	namespace math {

		template<class CLASS>
		inline const float * get_values(const CLASS & object){
			return glm::value_ptr(object);
		}

		inline Mat4x4 inverse  (const Mat4x4 & matrix) { return glm::inverse(matrix); }

		inline Mat4x4 transpose(const Mat4x4 & matrix) { return glm::transpose(matrix); }

		inline Mat4x4 translate(const Mat4x4 & matrix, const Vector3 & displacement) {
			return glm::translate(matrix, displacement);
		}

		inline Mat4x4 scale(const Mat4x4 & matrix, float scale) {
			return glm::scale(matrix, Vector3(scale, scale, scale));
		}

		inline Mat4x4 scale(const Mat4x4 & matrix, float scale_x, float scale_y, float scale_z) {
			return glm::scale(matrix, Vector3(scale_x, scale_y, scale_z));
		}

		inline Mat4x4 rotate_around_x(const Mat4x4 & matrix, float angle) {
			return glm::rotate(matrix, angle, glm::vec3(1.f, 0.f, 0.f));
		}

		inline Mat4x4 rotate_around_y(const Mat4x4 & matrix, float angle) {
			return glm::rotate(matrix, angle, glm::vec3(0.f, 1.f, 0.f));
		}

		inline Mat4x4 rotate_around_z(const Mat4x4 & matrix, float angle) {
			return glm::rotate(matrix, angle, glm::vec3(0.f, 0.f, 1.f));
		}

		inline Mat4x4 perspective(float fov, float nearp, float farp, float aspect_ratio) {
			return glm::perspective(fov, aspect_ratio, nearp, farp);
		}

		inline Mat4x4 look_at(const Vector3 & from, const Vector3 & to){
			return glm::lookAt(from, to, Vector3(0.f, 1.f, 0.f));
		}

		inline Vector2 extract_translation(const Mat3x3 & transformation){
			const  Vector3 & translation = transformation[2];
			return Vector2(translation[0], translation[1]);
		}

		inline Vector3 extract_translation(const Mat4x4 & transformation){
			const  Vector4 & translation = transformation[3];
			return Vector3(translation[0], translation[1], translation[2]);
		}

		inline Quaternion extract_rotation(const Mat4x4 & transformation){
			glm::vec3 scale;
			glm::quat rotation;
			glm::vec3 translation;
			glm::vec3 skew;
			glm::vec4 perspective;

			glm::decompose(transformation, scale, rotation, translation, skew, perspective);

			return rotation;
		}
	}
}

#endif