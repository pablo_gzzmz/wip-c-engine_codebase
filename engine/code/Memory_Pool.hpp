
#ifndef ENGINE_MEMORY_POOL_HEADER
#define ENGINE_MEMORY_POOL_HEADER

#include <stdint.h>

class Memory_Pool {
private:
	typedef uint8_t byte;

	byte * segment;
	byte * free;
	byte * end;

public:
	Memory_Pool(size_t size) {
		segment = new byte[size];
		free = segment;
		end = segment + size;
	}

	~Memory_Pool() {
		delete[] segment;
		segment = nullptr;
	}

	void * get_start() {
		return segment;
	}

	void * alloc(size_t size) {
		if (free + size < end) {
			byte * block = free;
			free += size;
			return block;
		}
		return nullptr;
	}
};

#endif
