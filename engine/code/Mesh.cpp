
#include "Mesh.hpp"

namespace rendering {
	typedef Render_Module::Mesh Mesh;

	Mesh::Mesh(vector<Vector3> &vertex_buffer, vector<Vector2> &uv_buffer, vector<Vector3> &normals_buffer, vector<GLubyte> & indices, GLenum _primitive_type, GLenum _indices_type){
		primitive_type = _primitive_type;
		indices_type   = _indices_type;

		assert(!vertex_buffer.empty());

		// ID creation
		glGenBuffers(VBO_COUNT, vbo_ids);
		glGenVertexArrays(1, &vao_id);

		glBindVertexArray(vao_id);

		// Bind VBO - coordinates 
		glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[VERTEX_VBO]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vector3) * vertex_buffer.size(), &vertex_buffer[0], GL_STATIC_DRAW);

		// Bind to VAO - coordinates
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

		if (!normals_buffer.empty()) {
			// Bind VBO - normals 
			glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[NORMALS_VBO]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vector3) * normals_buffer.size(), &normals_buffer[0], GL_STATIC_DRAW);

			// Bind to VAO - normals
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}

		if (!uv_buffer.empty()) {
			// Bind VBO - texture uvs 
			glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[TEXTUREUV_VBO]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vector2) * uv_buffer.size(), &uv_buffer[2][0], GL_STATIC_DRAW);

			// Bind to VAO - texture uvs
			glEnableVertexAttribArray(2);
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		}

		// Bind IBO
		if (!indices.empty()) {
			count = indices.size();
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[INDICES_IBO]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, count, &indices[0], GL_STATIC_DRAW);
		}
		else {
			count = vertex_buffer.size() + normals_buffer.size() + uv_buffer.size();
		}

		glBindVertexArray(0);
	}

	void Mesh::render() {
		/*if (material->is_textured())
		glBindTexture(GL_TEXTURE_2D, material->get_texture_id());*/

		glBindVertexArray(vao_id);

		if (indices_type != GL_NONE) {
			glDrawElements(primitive_type, count, indices_type, 0);
		}
		else {
			glDrawArrays (primitive_type, 0, count);
		}
		glBindVertexArray(0);
	}
}
