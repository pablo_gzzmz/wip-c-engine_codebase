

/*
	Pablo Gonzalez Practica 2 - 3D Avanzado ESNE

	Mesh class:
		-
*/

#ifndef ENGINE_MESH_HEADER
#define ENGINE_MESH_HEADER

#include "Render_Module.hpp"

namespace rendering {
	using std::vector;

	class Render_Module::Mesh {
		enum {
			VERTEX_VBO,
			NORMALS_VBO,
			TEXTUREUV_VBO,
			INDICES_IBO,
			VBO_COUNT
		};

	public:
		Mesh(vector<Vector3> &vertex_buffer, vector<Vector2> &uv_buffer, vector<Vector3> &normals_buffer, vector<GLubyte> & indices, GLenum _primitive_type = GL_TRIANGLES, GLenum _indices_type = GL_UNSIGNED_BYTE);

		~Mesh(){
			glDeleteVertexArrays(1, &vao_id);
			glDeleteBuffers     (VBO_COUNT, vbo_ids);
		}

	private:
		GLuint vbo_ids[VBO_COUNT];
		GLuint vao_id;
		size_t count;

		GLenum  primitive_type;
		GLenum  indices_type;

	public:
		void render();

		void set_primitive_type (GLenum new_primitive_type) { primitive_type = new_primitive_type; }

		void set_indices_type	(GLenum new_indices_type)   { indices_type   = new_indices_type;   }

		void specify_count(size_t _count) { count = _count; }
	};
}

#endif
