
#ifndef ENGINE_MESSAGE_HEADER
#define ENGINE_MESSAGE_HEADER

#include <unordered_map>
#include <string>
#include "Variant.hpp"

namespace engine_core {
	using std::string;
	using std::unordered_multimap;
	using std::make_pair;

	class Message {
	private:
		string id;

		unordered_multimap<string, Variant> parameters;

	public:
		Message(const string & _id) : id(_id) {}

	public:
		void add_parameter(const string & name, Variant value) {
			parameters.insert(make_pair(name, value));
		}

		const string get_id() const { return id; }
	};
}

#endif
