
#ifndef ENGINE_MESSAGING_MANAGER_HEADER
#define ENGINE_MESSAGING_MANAGER_HEADER

#include <vector>
#include <map>
#include <algorithm>
#include "Variant.hpp"

namespace engine_core {
	using std::string;
	using std::map;
	using std::vector;

	#pragma region Message struct
	struct Message {
		string id;

		map<string, Variant> parameters;

	public:
		Message(const string & _id) : id(_id) {}

	public:
		void add_parameter(const string & name, Variant value) {
			parameters.insert(std::make_pair(name, value));
		}
	};
	#pragma endregion

	struct Listener {
		virtual void handle_message(const Message & message) = 0;
	};

	class Messaging_Bus {
	private:
		map<string, vector<Listener *>> listeners;
	
	public:
		Messaging_Bus() {
			listeners = map<string, vector<Listener *>>();
		}

		void register_listener(const string & message_id, Listener * listener) {
			if (listeners.count(message_id) == 0) {
				listeners.insert(std::make_pair(message_id, vector<Listener *>()));
			}

			listeners.at(message_id).push_back(listener);
		}

		void unregister_listener(const string & message_id, Listener * listener) {
			vector<Listener *> * v = &listeners.at(message_id);
			std::remove(v->begin(), v->end(), listener);
		}

		void send(const Message & message) {
			if (listeners.count(message.id)) {
				for (auto listener_list : listeners) {
					for (auto listener : listener_list.second) {
						listener->handle_message(message);
					}
				}
			}
		}
	};
}

#endif