#define _CRT_SECURE_NO_WARNINGS // fscanf fopen

#include "Model.hpp"
#include "Mesh.hpp"
#include <string.h> // for memcmp


namespace rendering {
	typedef Render_Module::Model Model;

	Model::Model(const string & obj_file_path) {
		bool success = load_obj(obj_file_path);

		assert(success);
	}

	void Model::render() {
		material->use_shader();

		for (auto mesh = meshes.begin(); mesh != meshes.end(); ++mesh) {
			mesh->get()->render();
		}
	}

	bool Model::load_obj(const string & obj_file_path) {

		FILE * file = fopen(obj_file_path.c_str(), "r");

		assert(file != NULL);

		if (file == NULL) {
			printf("File is NULL \n");
			return false;
		}

		meshes.resize(1);
		size_t current_mesh = -1;

		vector<Vector3> out_vertices;
		vector<Vector2> out_uvs;
		vector<Vector3> out_normals;

		// VN = vertex / normals
		vector<Vector3> temporal_vertex;
		vector<Vector2> temporal_uvs;
		vector<Vector3> temporal_normals;
		vector<GLubyte> vertex_indices, normal_indices, uv_indices;

		while (1) {
			char line_header[128];
			// Read the first word of the line
			int res = fscanf(file, "%s", line_header);
			if (res == EOF) // Break out if end of file
				break; 

			if (strcmp(line_header, "object") == 0) {
				if (current_mesh > -1) {
					// CREATION OF MESH DATA GIVEN BUFFERS
					create_mesh(out_vertices, out_uvs, out_normals, current_mesh);
				}

				temporal_vertex.clear();
				temporal_uvs.clear();
				temporal_normals.clear();
				vertex_indices.clear();
				normal_indices.clear();
				uv_indices.clear();

				// Increase one mesh for every object contained in the .obj
				current_mesh++;
				meshes.resize(current_mesh+1);
			}

			#pragma region Vertice coordinates
			else if (strcmp(line_header, "v") == 0) {
				Vector3 vertex;
				fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
				temporal_vertex.push_back(vertex);
			}
			#pragma endregion

			#pragma region Normals
			else if (strcmp(line_header, "vn") == 0) {
				Vector3 normal;
				fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
				temporal_normals.push_back(normal);
			}
			#pragma endregion

			#pragma region Texture coordinates
			else if (strcmp(line_header, "vt") == 0) {
				Vector2 texture_uv;
				fscanf(file, "%f %f\n", &texture_uv.x, &texture_uv.y);
				temporal_uvs.push_back(texture_uv);
			}
			#pragma endregion

			#pragma region Indices
			else if (strcmp(line_header, "f") == 0) {
				// Get vertices, uv and normal indices for a face
				int vertex_index[3], uv_index[3], normal_index[3];
				fscanf(
					file, "%d/%d/%d %d/%d/%d %d/%d/%d\n",
					&vertex_index[0], &uv_index[0], &normal_index[0],
					&vertex_index[1], &uv_index[1], &normal_index[1],
					&vertex_index[2], &uv_index[2], &normal_index[2]
				);
				vertex_indices.push_back(vertex_index[0]);
				vertex_indices.push_back(vertex_index[1]);
				vertex_indices.push_back(vertex_index[2]);
				uv_indices.push_back(    uv_index[0]);
				uv_indices.push_back(    uv_index[1]);
				uv_indices.push_back(    uv_index[2]);
				normal_indices.push_back(normal_index[0]);
				normal_indices.push_back(normal_index[1]);
				normal_indices.push_back(normal_index[2]);
			}
			#pragma endregion

			// For each vertex of each triangle
			for(unsigned int i=0; i < vertex_indices.size(); i++){

				// Get the indices of its attributes
				unsigned int vertex_index = vertex_indices[i];
				unsigned int uv_index     = uv_indices[i];
				unsigned int normal_index = normal_indices[i];

				// Get the attributes thanks to the index
				Vector3 vertex = temporal_vertex [vertex_index-1];
				Vector2 uv     = temporal_uvs    [uv_index-1];
				Vector3 normal = temporal_normals[normal_index-1];

				// Put the attributes in buffers
				out_vertices.push_back(vertex);
				out_uvs     .push_back(uv);
				out_normals .push_back(normal);
			}
		}

		// CREATION OF MESH DATA GIVEN BUFFERS - Final mesh (or unique mesh)
		create_mesh(out_vertices, out_uvs, out_normals, current_mesh);
		return true;
	}

	void Render_Module::Model::create_mesh(vector<Vector3> vertex_buffer, vector<Vector2> uvs_buffer, vector<Vector3> normals_buffer, int index) {
		vector<GLubyte> indices;
		vector<Vector3> indexed_vertex_buffer;
		vector<Vector2> indexed_uvs_buffer;
		vector<Vector3> indexed_normals_buffer;

		indexVBO(vertex_buffer, uvs_buffer, normals_buffer, indices, indexed_vertex_buffer, indexed_uvs_buffer, indexed_normals_buffer);

		meshes.at(index).reset(new Mesh(indexed_vertex_buffer, indexed_uvs_buffer, indexed_normals_buffer, indices));
	}

	#pragma region VBO INDEXING
	bool is_near(float v1, float v2){
		return fabs( v1-v2 ) < 0.01f;
	}

	bool is_similar( 
		Vector3 & in_vertex,
		Vector2 & in_uv,
		Vector3 & in_normal,
		vector<Vector3> & out_vertices,
		vector<Vector2> & out_uvs,
		vector<Vector3> & out_normals,
		GLubyte & result
	){
		// Lame linear search
		for ( unsigned int i=0; i < out_vertices.size(); i++ ){
			if (
				is_near(in_vertex.x , out_vertices[i].x) &&
				is_near(in_vertex.y , out_vertices[i].y) &&
				is_near(in_vertex.z , out_vertices[i].z) &&
				is_near(in_uv.x     , out_uvs     [i].x) &&
				is_near(in_uv.y     , out_uvs     [i].y) &&
				is_near(in_normal.x , out_normals [i].x) &&
				is_near(in_normal.y , out_normals [i].y) &&
				is_near(in_normal.z , out_normals [i].z)
				){
				result = i;
				return true;
			}
		}
		// No other vertex could be used instead.
		// Looks like we'll have to add it to the VBO.
		return false;
	}

	struct Packed_Vertex{
		Vector3 position;
		Vector2 uv;
		Vector3 normal;
		bool operator<(const Packed_Vertex that) const{
			return memcmp((void*)this, (void*)&that, sizeof(Packed_Vertex))>0;
		};
	};

	bool get_similar_vertex_index_fast( 
		Packed_Vertex & packed, 
		map<Packed_Vertex, GLubyte> & vertex_to_out_index,
		GLubyte & result
	){
		map<Packed_Vertex, GLubyte>::iterator it = vertex_to_out_index.find(packed);
		if ( it == vertex_to_out_index.end() ){
			return false;
		}else{
			result = it->second;
			return true;
		}
	}

	void Render_Module::Model::indexVBO(
		vector<Vector3> & in_vertices,
		vector<Vector2> & in_uvs,
		vector<Vector3> & in_normals,
		vector<GLubyte> & out_indices,
		vector<Vector3> & out_vertices,
		vector<Vector2> & out_uvs,
		vector<Vector3> & out_normals
	){
		map<Packed_Vertex, GLubyte> vertex_to_out_index;

		// For each input vertex
		for (unsigned int i=0; i < in_vertices.size(); i++ ){

			Packed_Vertex packed = {in_vertices[i], in_uvs[i], in_normals[i]};

			// Try to find a similar vertex in out_XXXX
			GLubyte index;
			bool found = get_similar_vertex_index_fast( packed, vertex_to_out_index, index);

			if (found){ // A similar vertex is already in the VBO, use it instead !
				out_indices.push_back(index);
			}else{ // If not, it needs to be added in the output data.
				out_vertices.push_back(in_vertices[i]);
				out_uvs     .push_back(in_uvs	  [i]);
				out_normals .push_back(in_normals [i]);
				GLubyte newindex = static_cast<GLubyte>(out_vertices.size() - 1);
				out_indices .push_back(newindex);
				vertex_to_out_index[packed] = newindex;
			}
		}
	}
	#pragma endregion
}