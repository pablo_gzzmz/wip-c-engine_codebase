
#ifndef ENGINE_MODEL_HEADER
#define ENGINE_MODEL_HEADER

#include "Render_Module.hpp"
#include "Material.hpp"

namespace rendering {
	using std::shared_ptr;
	using std::vector;

	class Render_Module::Model {
	private:
		vector<shared_ptr<Mesh>> meshes;
		shared_ptr<Material>     material;

	public:
		Model(const string & obj_file_path);

	public:
		void	   set_material(Shader_Program * _shader) { material.reset(new Material(_shader)); }
		Material * get_material() { return material.get(); }

		void render();

	private:
		bool load_obj(const std::string & obj);
	
		void create_mesh(
			vector<Vector3> vertex_buffer, 
			vector<Vector2> uvs_buffer, 
			vector<Vector3> normals_buffer,
			int index);

		void indexVBO(
			vector<Vector3> & in_vertices,
			vector<Vector2> & in_uvs,
			vector<Vector3> & in_normals,

			vector<GLubyte> & out_indices,
			vector<Vector3> & out_vertices,
			vector<Vector2> & out_uvs,
			vector<Vector3> & out_normals
		);
	};
}

#endif
