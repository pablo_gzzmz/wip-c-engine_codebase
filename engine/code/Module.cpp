
#include "Module.hpp"
#include "Scene.hpp"

namespace engine_core {

	Module::Module(string _name, Scene * _parent_scene, Task * _task) { 
		name		 = _name;
		parent_scene = _parent_scene;

		task = _task;

		parent_scene->add_module(this, name);
	}
}