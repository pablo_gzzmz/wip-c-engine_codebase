
#ifndef ENGINE_MODULE_HEADER
#define ENGINE_MODULE_HEADER

#include "Task.hpp"
#include <string>

namespace engine_core {
	using std::string;

	class Scene;
	class Module {

	protected:
		string name;

		Scene * parent_scene;

		Task  * task;

	protected:
		// Modules' parent scene adds their tasks to the kernel automatically.
		Module(string _name, Scene * _parent_scene, Task * _task);

	public:
		const string  get_name        () { return name; }

		const Scene * get_parent_scene() { return parent_scene; }

			  Task  * get_task        () { return task;}
	};
};

#endif