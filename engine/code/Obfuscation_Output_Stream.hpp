
#ifndef ENGINE_OBFUSCATION_OUTPUT_STREAM
#define ENGINE_OBFUSCATION_OUTPUT_STREAM

#include "Output_Stream.hpp"
#include <vector>

namespace engine_core {

	class Obfuscation_Output_Stream : public Output_Stream {
	private:
		Output_Stream & stream;
		char			key;

	public:
		Obfuscation_Output_Stream(Output_Stream & _stream, char _key) : stream(_stream) {
			key = _key;
		}

		bool write(const char * buffer, size_t amount) {
			std::vector<char> obfuscated_buffer(amount);

			for (size_t i = 0; i < amount; ++i) {
				obfuscated_buffer[i] = buffer[i] ^ key;
			}

			stream.write(obfuscated_buffer.data(), amount);
		}
	};
}

#endif
