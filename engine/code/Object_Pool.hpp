
#ifndef ENGINE_OBJECT_POOL_HEADER
#define ENGINE_OBJECT_POOL_HEADER

namespace engine {
	template<class CLASS>
	class Object_Pool {
	private:
		Memory_Pool memory_pool;

		size_t count;
	public:
		Object_Pool(size_t number_of_objects) : memory_pool(sizeof(Class) * number_of_objects) { count = 0; }

		~Object_Pool() {
			CLASS * it = reinterpret_cast<CLASS *> (memory_pool.get_start());
			while (count--) {
				(it++)->~CLASS();
			}
		}

		CLASS * alloc{
			CLASS * object = reinterpret_cast<CLASS *> (memory_pool.alloc(sizeof(CLASS)));

		if (object) {
			count++;
			return new (object) CLASS;
		}

		return nullptr;
		}
	};
}

#endif


