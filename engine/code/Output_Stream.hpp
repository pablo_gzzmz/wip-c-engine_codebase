
#ifndef ENGINE_OUTPUT_STREAM_HEADER
#define ENGINE_OUTPUT_STREAM_HEADER

namespace engine_core {

	class Output_Stream {
	public:
		virtual bool write(char * buffer, size_t amount) = 0;

	};

}

#endif
