
#ifndef ENGINE_PHYSICS_COMPONENT_HEADER
#define ENGINE_PHYSICS_COMPONENT_HEADER

#include "Physics_Module.hpp"
#include "Collider.hpp"

namespace physics {

	class Physics_Module::Physics_Component : public engine_core::Component {
	private:
		shared_ptr<btRigidBody>	rigidbody;

		btTransform		   physics_transform;
		vector<Collider *> colliders;

	public:
		bool initialize() override {
			physics_transform.setIdentity();
			physics_transform.setOrigin(btVector3(0, 0, 0));

			btDefaultMotionState motion_state = btDefaultMotionState(physics_transform);
			btRigidBody::btRigidBodyConstructionInfo info = btRigidBody::btRigidBodyConstructionInfo(0, &motion_state, collider->get_shape(), btVector3(0, 0, 0));
		    rigidbody.reset(new btRigidBody(info));
		}

		const btRigidBody * get_rigidbody() { return	rigidbody.get(); }

		const btTransform * get_physics_transform() { return &physics_transform; }
	};

}

#endif