
#ifndef ENGINE_PHYSICS_MODULE_HEADER
#define ENGINE_PHYSICS_MODULE_HEADER

#include "Module.hpp"
#include "Component.hpp"
#include <btBulletDynamicsCommon.h>
#include <memory>
#include <vector>

using namespace engine_core;

namespace physics {
	using std::shared_ptr;
	using std::vector;

	class Physics_Module : public engine_core::Module {
		
		class Physics_World;
		class Physics_Component;
		class Collider;

	};
}

#endif
