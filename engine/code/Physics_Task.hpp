
#ifndef ENGINE_PHYSICS_TASK_HEADER
#define ENGINE_PHYSICS_TASK_HEADER

#include "Task.hpp"

namespace physics {
	class Physics_Task : public engine_core::Task {

	public:
		int priority	() const	 override { return 4; }

		bool initialize	()			 override { return false;}

		bool step		(float time) override;

		bool finalize	()			 override { return false;}
	};
}

#endif
