
#ifndef ENGINE_PHYSICS_WORLD_HEADER
#define ENGINE_PHYSICS_WORLD_HEADER

#include "Physics_Module.hpp"

namespace physics {

	class Physics_Module::Physics_World {
	private:
		btDefaultCollisionConfiguration			collision_configuration;
		btCollisionDispatcher					collision_dispatcher;
		btDbvtBroadphase						overlapping_pair_cache;
		btSequentialImpulseConstraintSolver		constraint_solver;

		btDiscreteDynamicsWorld					dynamics_world;

		vector<btRigidBody *>					rigidbodies;

	public:
		static Physics_World & instance() {
			static Physics_World _instance;
			return _instance;
		}

	private:
		Physics_World() : collision_dispatcher(&collision_configuration),
			dynamics_world(&collision_dispatcher,
				&overlapping_pair_cache,
				&constraint_solver,
				&collision_configuration) {
			set_gravity();
		}

	public:
		void set_gravity(float gravity = 9.8f) {
			dynamics_world.setGravity(btVector3(0, gravity, 0));
		}

		void add_rigidbody(btRigidBody * new_rigidbody) {
			rigidbodies.push_back(new_rigidbody);
		}
	};

}

#endif
