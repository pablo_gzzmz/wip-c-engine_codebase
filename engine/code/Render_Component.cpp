
#include "Model.hpp"
#include "Render_Component.hpp"
#include "Entity.hpp"
#include "Transform_Component.hpp"

namespace rendering {
	typedef Render_Module::Model Model;
	typedef Render_Module::Render_Component Render_Component;

	vector<Render_Component *> Render_Component::renderers_cache;

	bool Render_Component::initialize() {
		renderers_cache.push_back(this);

		return true;
	}

	void Render_Component::render(const Mat4x4 view_matrix) {
		if (model != nullptr) {
			model->get_material()->use_shader();
			Shader_Program * shader = model->get_material()->get_shader();
			shader->set_uniform(model_view_matrix_id, view_matrix * entity->get_transform()->get_transformation());
			model->render();
		}
	}

	void Render_Component::load_model(const string & path) {
		model.reset(new Model(path));
	}

}