
#ifndef ENGINE_RENDER_COMPONENT_HEADER
#define ENGINE_RENDER_COMPONENT_HEADER

#include <memory>
#include "Render_Module.hpp"
#include "Component.hpp"
#include "Model.hpp"

namespace rendering {

	class Render_Module::Render_Component : public Component{
	private:
		static vector<Render_Component *> renderers_cache;

		shared_ptr<Model> model;

		GLuint model_view_matrix_id;

	public:
		Render_Component(Entity * _parent_entity) : Component(_parent_entity) {}

	public:
		bool	initialize	   () override;

		bool	duplicable	   () override { return false; }

		string	component_name () override { return "renderer"; }

		bool    parse_property (const string & name, const string & value) override { return true;}

		void render(const Mat4x4 view_matrix);

		void load_model(const string & path);

		void set_material(Shader_Program * s) {
			model->set_material(s);

			model_view_matrix_id = model->get_material()->get_shader()->get_uniform_id("model_view_matrix");
		}

	public:
		Model * get_model() { return model.get(); }

	public:
		static vector<Render_Component *>::iterator get_renderers_cache_begin() { return renderers_cache.begin(); }
		static vector<Render_Component *>::iterator get_renderers_cache_end  () { return renderers_cache.end  (); }
	};
}

#endif