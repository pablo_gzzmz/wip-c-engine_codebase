
#include "Window.hpp"
#include "Render_Module.hpp"
#include "Camera_Component.hpp"
#include "Entity.hpp"
#include "Transform_Component.hpp"
#include "Render_Component.hpp"
#include "Scene.hpp"

namespace rendering {
	typedef Render_Module Render_Module;
	typedef Render_Module::Camera_Component Camera;

	// For easy main camera access
	Camera * main_cam() { return Camera::get_main(); }

	Render_Module::Render_Module(string name, Scene * _parent_scene) : render_task(this), Module(name, _parent_scene, &render_task) {
		GLenum glew_initialization =  glewInit ();
		assert(glew_initialization == GLEW_OK);

		glEnable      (GL_BLEND);
		glEnable      (GL_LINE_SMOOTH);
		glHint        (GL_LINE_SMOOTH_HINT, GL_NICEST);
		glBlendFunc   (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	bool Render_Module::render(string & error) {

		if (!Camera::can_render()) {
			error = "No active cameras";
			return false;
		}

		// BASE BACKGROUND

		glClearColor(main_cam()->view_color_r(), main_cam()->view_color_g(), main_cam()->view_color_b(), 1.f);

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		// CLEAR

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// ALL RENDERERS ITERATION

		Shader_Program * shader;

		Mat4x4 view_matrix = main_cam()->get_entity()->get_transform()->get_transformation ();

		vector<Render_Component *>::iterator it     = Render_Component::get_renderers_cache_begin();
		vector<Render_Component *>::iterator it_end = Render_Component::get_renderers_cache_end  ();
		for (it; it != it_end; ++it) {
			shader = (*it)->get_model()->get_material()->get_shader();
			shader->set_projection_matrix_id(main_cam()->get_projection_matrix());

			(*it)->render(view_matrix);
		}


		parent_scene->get_window()->swap_buffer();

		error = "";
		return true;
	}

	bool Render_Module::Render_Task::step(float time) {
		static string error_temp = "";
		static string error_log  = "";

		// Execute render and check for error
		bool render_successful = render_module->render(error_log);

		if (!render_successful) {
			// If rendering fails with a different error, notify
			if (error_log != error_temp) {
				error_temp = error_log;
				printf(error_log.c_str());
			}
		}

		return true;
	}

	const string Render_Module::basic_vertex_shader =
		"#version 330\n"
		""
		"uniform mat4 model_view_matrix;"
		"uniform mat4 projection_matrix;"
		""
		"layout (location = 0) in vec3 vertex_coordinates;"
		"layout (location = 1) in vec3 vertex_color;"
		""
		"out vec3 front_color;"
		""
		"void main()"
		"{"
		"   gl_Position = projection_matrix * model_view_matrix * vec4(vertex_coordinates, 1.0);"
		"   front_color = vec3(1.0, 0.0, 0.0);"
		"}";

	const string Render_Module::basic_fragment_shader =
		"#version 330\n"
		""
		"in  vec3    front_color;"
		"out vec4 fragment_color;"
		""
		"void main()"
		"{"
		"    fragment_color = vec4(front_color, 1.0);"
		"}";
}