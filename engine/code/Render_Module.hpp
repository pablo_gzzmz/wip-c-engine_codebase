
#ifndef ENGINE_RENDER_MODULE_HEADER
#define ENGINE_RENDER_MODULE_HEADER

#include <vector>
#include <map>
#include <unordered_map>
#include <list>
#include <cassert>
#include <memory>
#include <GL/glew.h>
#include "SFML/OpenGL.hpp"
#include <glm/gtc/type_ptr.hpp>
#include "Math.hpp"
#include "Module.hpp"
#include "Color.hpp"
#include "Component.hpp"

using namespace engine_core;

namespace rendering {
	using std::list;
	using std::map;
	using std::vector;

	class Render_Module : public Module {
	public:
		class Shader_Program;
		class Render_Component;
		class Camera_Component;
		class Mesh;
		class Material;
		class Model;

		class Render_Task : public Task {
		private:
			Render_Module * render_module;

		public:
			Render_Task(Render_Module * _render_module) : render_module(_render_module) {}

		public:
			int priority	() const	 override { return 3; }

			bool initialize	()			 override { return true; }

			bool step		(float time) override;

			bool finalize  ()			 override { return true; }
		};

	private:
		Render_Task render_task;

	public:
		Render_Module(string name, Scene * _parent_scene);

	public:
		bool render(string & error);

	public:
		static const string basic_vertex_shader;
		static const string basic_fragment_shader;
	};
}

#endif
