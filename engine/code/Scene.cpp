
#include "Scene.hpp"
#include "Entity.hpp"
#include "Timer.hpp"
#include "Module.hpp"
#include "Kernel.hpp"
#include "Transform_Component.hpp"

namespace engine_core {
	
	Scene::Scene(string _name, Kernel * _kernel) {
		name   = _name;
		kernel = _kernel;

		messaging_bus.reset(new Messaging_Bus());

		clock.reset(new Timer());

		// Since this is root entity, notify in construction to not add to scene
		root.reset(new Entity(_name, this, false));

		clock->initialize();
	}

	void Scene::update() { 
		root->hierarchy_update(root->get_transform());
	}

	bool Scene::add_module(Module * new_module, const string & name) {
		if (modules.count(name) > 0) {
			printf("Repeated module name");
			return false;
		}

		modules.insert(std::make_pair(name, new_module));

		// Add the task of the module to the kernel used by the scene
		kernel->add_task(new_module->get_task());

		return true;
	}

	void Scene::add_entity(Entity * entity) {
		root->add_child(entity);
	}

	float Scene::delta_time() const { return kernel->get_delta_time(); }
}