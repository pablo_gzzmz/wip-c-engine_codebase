
#ifndef ENGINE_SCENE_HEADER
#define ENGINE_SCENE_HEADER

#include <unordered_map>
#include <memory>
#include <string>

namespace engine_core {
	using std::unordered_map;
	using std::shared_ptr;
	using std::string;

	class Module;
	class Window;
	class Messaging_Bus;
	class Kernel;
	class Entity;
	class Timer;

	class Scene {
	private:
		string name;

		unordered_map<string, Module *> modules;

		Kernel  * kernel;

		Window	* scene_window;

		shared_ptr<Entity> root;

		shared_ptr<Timer> clock;

		shared_ptr<Messaging_Bus> messaging_bus;

	public:
		Scene(string _name, Kernel * _kernel);

	public:
		void update();

	public:
		const string    get_name		  () const { return name; }

		Kernel		  * get_kernel		  () const { return kernel; }

		Messaging_Bus * get_messaging_bus () const { return messaging_bus.get(); }

		Timer		  * get_clock		  () const { return clock.get(); }

		float			delta_time		  () const;

		Window	      * get_window		  () const { return scene_window; }

		void set_window(Window * window) { scene_window = window; }

	public:
		bool add_module(Module * new_module, const string & name);

		void add_entity(Entity * entity);
	};
}

#endif

