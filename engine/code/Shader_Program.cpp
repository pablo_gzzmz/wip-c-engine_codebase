
#include "Shader_Program.hpp"
#include <iostream>

namespace rendering {
	typedef Render_Module::Shader_Program Shader_Program;

	GLuint Shader_Program::active_shader_program_id = 0;

	Shader_Program::Shader_Program(const string & _name) { 
		name  = _name; 
		id    = 0;

		vertex_shader.code   = Render_Module::basic_vertex_shader;
		fragment_shader.code = Render_Module::basic_fragment_shader;

		compile();
	}

	Shader_Program::Shader_Program(const string & _name, const string & vertex_code, const string & fragment_code) { 
		name  = _name; 
		id    = 0;

		  vertex_shader.code = vertex_code;
		fragment_shader.code = fragment_code;

		compile();
	}

	void Shader_Program::compile() {
		GLint success = GL_FALSE;

		// Vertex and fragment shader

		  vertex_shader.id = glCreateShader(GL_VERTEX_SHADER);
		fragment_shader.id = glCreateShader(GL_FRAGMENT_SHADER);


		const char *   vertex_shaders_code[] = {   vertex_shader.code.c_str()};
		const char * fragment_shaders_code[] = { fragment_shader.code.c_str() };
		const GLint    vertex_shaders_size[] = { static_cast<GLint>(  vertex_shader.code.size()) };
		const GLint  fragment_shaders_size[] = { static_cast<GLint>(fragment_shader.code.size()) };

		glShaderSource(  vertex_shader.id, 1,   vertex_shaders_code, vertex_shaders_size  );
		glShaderSource(fragment_shader.id, 1, fragment_shaders_code, fragment_shaders_size);


		glCompileShader(  vertex_shader.id);
		glCompileShader(fragment_shader.id);

		// Check compilation success

		glGetShaderiv(vertex_shader.id, GL_COMPILE_STATUS, &success);
		if (!success) show_compilation_error(vertex_shader.id);

		glGetShaderiv(fragment_shader.id, GL_COMPILE_STATUS, &success);
		if (!success) show_compilation_error(fragment_shader.id);

		
		// Shader program

		id = glCreateProgram();
		assert(id != 0);
		glAttachShader(id,   vertex_shader.id);
		glAttachShader(id, fragment_shader.id);
		glLinkProgram(id);

		// Check linkage success

		glGetProgramiv(id, GL_LINK_STATUS, &success);
		if (!success) show_linkage_error(id);
		else link_completed = true;

		// Delete linked shaders

		glDeleteShader(  vertex_shader.id);
		glDeleteShader(fragment_shader.id);

		// Get generic ids

		projection_matrix_id = glGetUniformLocation (id, "projection_matrix");
	}

	void Shader_Program::show_compilation_error(GLuint shader_id) {
		string info_log;
		GLint  info_log_length;

		glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_log_length);

		info_log.resize(info_log_length);

		glGetShaderInfoLog(shader_id, info_log_length, NULL, &info_log.front());

		std::cerr << info_log.c_str() << std::endl;

		#ifdef _MSC_VER
		OutputDebugStringA(info_log.c_str());
		#endif

		assert(false);
	}

	void Shader_Program::show_linkage_error(GLuint program_id) {
		string info_log;
		GLint  info_log_length;

		glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_log_length);

		info_log.resize(info_log_length);

		glGetProgramInfoLog(program_id, info_log_length, NULL, &info_log.front());

		std::cerr << info_log.c_str() << std::endl;

		#ifdef _MSC_VER
		OutputDebugStringA(info_log.c_str());
		#endif

		assert(false);
	}
}