
#ifndef ENGINE_SHADER_PROGRAM_HEADER
#define ENGINE_SHADER_PROGRAM_HEADER

#include "Render_Module.hpp"

namespace rendering {

	class Render_Module::Shader_Program {
	private:
		static GLuint  active_shader_program_id;

	protected:
		string name;

		GLuint id;

		struct Shader {
			GLuint id;
			string code;
		};

		Shader vertex_shader;
		Shader fragment_shader;

		GLuint projection_matrix_id;

		bool   link_completed;
		string error_log;

	public:
		Shader_Program(const string & _name);
		Shader_Program(const string & _name, const string & vertex_code, const string & fragment_code);

	private:
		void compile();

	public:
		static void disable() {
			glUseProgram(0);
		}

	public:
		const string get_name  () const { return name; }

		const bool   is_usable () const { return link_completed; }

	public:
		void use() const {
			assert(is_usable());

			if (id != active_shader_program_id){
				glUseProgram(id);
				active_shader_program_id = id;
			}
		}

	private:
		static void show_compilation_error(GLuint shader_id);
		static void show_linkage_error	  (GLuint program_id);

	public:
		// Generic id
		void set_projection_matrix_id(const Mat4x4 & matrix) const { glUniformMatrix4fv (projection_matrix_id, 1, GL_FALSE, math::get_values (matrix)); }

		GLint get_uniform_id (const char * identifier) const {
			assert (is_usable ());

			GLint uniform_id = glGetUniformLocation (id, identifier);

			assert (uniform_id != -1);

			return (uniform_id);
		}

		void set_uniform (GLint uniform_id, const GLint    & value     ) const { glUniform1i  (uniform_id, value); }
		void set_uniform (GLint uniform_id, const GLuint   & value     ) const { glUniform1ui (uniform_id, value); }
		void set_uniform (GLint uniform_id, const float    & value     ) const { glUniform1f  (uniform_id, value); }
		void set_uniform (GLint uniform_id, const float   (& vector)[2]) const { glUniform2f  (uniform_id, vector[0], vector[1]); }
		void set_uniform (GLint uniform_id, const float   (& vector)[3]) const { glUniform3f  (uniform_id, vector[0], vector[1], vector[2]); }
		void set_uniform (GLint uniform_id, const float   (& vector)[4]) const { glUniform4f  (uniform_id, vector[0], vector[1], vector[2], vector[3]); }
		void set_uniform (GLint uniform_id, const Vector2  & vector    ) const { glUniform2f  (uniform_id, vector[0], vector[1]); }
		void set_uniform (GLint uniform_id, const Vector3  & vector    ) const { glUniform3f  (uniform_id, vector[0], vector[1], vector[2]); }
		void set_uniform (GLint uniform_id, const Vector4  & vector    ) const { glUniform4f  (uniform_id, vector[0], vector[1], vector[2], vector[3]); }
		void set_uniform (GLint uniform_id, const Mat2x2   & matrix    ) const { glUniformMatrix2fv (uniform_id, 1, GL_FALSE, math::get_values (matrix)); }
		void set_uniform (GLint uniform_id, const Mat3x3   & matrix    ) const { glUniformMatrix3fv (uniform_id, 1, GL_FALSE, math::get_values (matrix)); }
		void set_uniform (GLint uniform_id, const Mat4x4   & matrix    ) const { glUniformMatrix4fv (uniform_id, 1, GL_FALSE, math::get_values (matrix)); }
	};
}

#endif
