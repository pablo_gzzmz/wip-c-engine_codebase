
#ifndef ENGINE_TASK_HEADER
#define ENGINE_TASK_HEADER

namespace engine_core {

	class Kernel;

	class Task {
	protected:
		Kernel * kernel;

	public:
		Task() {}

	public:
		// Should be used by the kernel itself when adding a task
		void set_kernel(Kernel * _kernel) {
			kernel = _kernel; 
		}

		virtual int  priority	() const	 = 0;

		virtual bool initialize	()			 = 0;
		virtual bool step	    (float time) = 0;
		virtual bool finalize	()			 = 0;

	public:
		bool operator < (const Task & other) {
			return this->priority() < other.priority();
		}
	};
}

#endif