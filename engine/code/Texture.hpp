
#ifndef ENGINE_TEXTURE_HEADER
#define ENGINE_TEXTURE_HEADER

#include <map>
#include <string>
#include "Color_Buffer_Rgba8888.hpp"
#include "Color.hpp"

namespace rendering {

	using std::map;
	using std::string;
	typedef engine_core::Color Color;

	class Texture2D {
	private:
		Color_Buffer_Rgba8888 buffer;

	public:
		Texture2D(size_t width, size_t height) : buffer(width, height) {}

		int bits_per_color() { return buffer.bits_per_color(); }

		Color * colors() { return (buffer.colors()); }
	};
}

#endif
