

#ifndef ENGINE_TIMER_HEADER
#define ENGINE_TIMER_HEADER

#include <SDL.h>
#include <cstdint>

namespace engine_core {
	
	class Timer {
	private:
		uint64_t start_ticks;

	public:
		Timer() {}

	public:
		void initialize	() { start_ticks = SDL_GetPerformanceCounter(); }

		void restart	() { start_ticks = SDL_GetPerformanceCounter(); }


		float elapsed_seconds() {
			double elapsed_ticks = double(SDL_GetPerformanceCounter() - start_ticks);

			return float(elapsed_ticks / SDL_GetPerformanceFrequency());
		}
	};
}

#endif