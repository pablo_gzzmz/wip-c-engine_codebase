
#include "Transform_Component.hpp"

namespace engine_core {
	bool Transform_Component::initialize() {
		reset_transformation(1.f);
		return true;
	}

	bool Transform_Component::parse_property(const string & name, const string & value) {
		
		return true;
	}

	void Transform_Component::update(Transform_Component * parent) {
		if (parent != nullptr) {
			transformation = parent->get_transformation() * local_transformation;
		}

		position = math::extract_translation(transformation);
		rotation = math::extract_rotation(transformation);
		euler_rotation = glm::eulerAngles(rotation);
	}
}