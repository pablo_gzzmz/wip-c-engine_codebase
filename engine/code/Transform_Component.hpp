
#ifndef ENGINE_TRANSFORM_COMPONENT
#define ENGINE_TRANSFORM_COMPONENT

#include "Component.hpp"
#include "Math.hpp"

namespace engine_core {

	class Transform_Component : public Component {
	private:
		Vector3		position;
		Vector3		euler_rotation;
		Quaternion	rotation;
		Vector3     scale;

		Mat4x4		transformation;
		Mat4x4		local_transformation;
		
	public:
		Transform_Component(Entity * _entity) : Component(_entity) {}

	public:
		bool	initialize		()											override;

		bool	parse_property	(const string & name, const string & value) override;

		bool	duplicable		()											override { return false; }

		string	component_name	()											override { return "transform"; }

	public:
		void update (Transform_Component * parent);

		void set_transformation(const Mat4x4 & new_transformation){
			local_transformation = new_transformation;

			position = math::extract_translation(transformation);
			rotation = math::extract_rotation(transformation);
			euler_rotation = glm::eulerAngles(rotation);
		}

		const Mat4x4 & get_transformation() const { return transformation; }

		Mat4x4 get_inverse_transformation() const { return math::inverse(transformation); }

		const Vector3	 get_position	   () { return position; }
		const Quaternion get_rotation	   () { return rotation; }
		const Vector3    get_euler_rotation() { return euler_rotation; }
		const Vector3    get_scale		   () { return scale; }

		const Vector3 forward() {
			float x2 = 2.0f * rotation.x;
			float y2 = 2.0f * rotation.y;
			float z2 = 2.0f * rotation.z;
			float x2w = x2 * rotation.w;
			float y2w = y2 * rotation.w;
			float x2x = x2 * rotation.x;
			float z2x = z2 * rotation.x;
			float y2y = y2 * rotation.y;
			float z2y = z2 * rotation.y;
			return Vector3(z2x + y2w, z2y - x2w, (1.0f - (x2x + y2y)));
		}

		void reset_transformation(float new_scale) {
			local_transformation = Mat4x4();

			position = Vector3();
			rotation = Quaternion();
			euler_rotation = Vector3();
			scale = Vector3(new_scale, new_scale, new_scale);
		}

		void translate(const Vector3 & displacement){
			local_transformation = math::translate(local_transformation, displacement);

			position += displacement;
		}

		void set_scale(float _scale){
			local_transformation = math::scale(local_transformation, _scale);
			scale = Vector3(_scale, _scale, _scale);
		}

		void set_scale(float scale_x, float scale_y, float scale_z) {
			local_transformation = math::scale(local_transformation, scale_x, scale_y, scale_z);
			scale = Vector3(scale_x, scale_y, scale_z);
		}

		void rotate_around_x(float angle){
			local_transformation = math::rotate_around_x(local_transformation, angle);

			rotation = math::extract_rotation(transformation);
			euler_rotation = glm::eulerAngles(rotation);
		}

		void rotate_around_y(float angle){
			local_transformation = math::rotate_around_y(local_transformation, angle);

			rotation = math::extract_rotation(transformation);
			euler_rotation = glm::eulerAngles(rotation);
		}

		void rotate_around_z(float angle){
			local_transformation = math::rotate_around_z(local_transformation, angle);

			rotation = math::extract_rotation(transformation);
			euler_rotation = glm::eulerAngles(rotation);
		}

		void look_at(const Vector3 & where){
			local_transformation = math::look_at(math::extract_translation(local_transformation), where);

			rotation = math::extract_rotation(transformation);
			euler_rotation = glm::eulerAngles(rotation);
		}
	};
}

#endif
