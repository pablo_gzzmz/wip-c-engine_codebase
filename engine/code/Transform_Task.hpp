
#ifndef ENGINE_TRANSFORM_TASK_HEADER
#define ENGINE_TRANSFORM_TASK_HEADER

#include "Task.hpp"
#include "Entity.hpp"

namespace engine_core {

	class Transform_Task : public Task {
		Entity * root;

	public:
		int priority() const { return 5; }

		bool initialize() override {
			return false;
		}

		bool step(float time) override {
			return false;
		}

		bool finalize() override {
			return false;
		}
	};
}

#endif
