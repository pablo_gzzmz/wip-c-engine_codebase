
#ifndef ENGINE_UPDATE_TASK
#define ENGINE_UPDATE_TASK

#include "Task.hpp"
#include "Scene.hpp"

namespace engine_core {

	class Scene;

	class Update_Task : public Task{
	private:
		Scene * scene;
	
	public:
		Update_Task(Scene * _scene) : scene(_scene), Task() {}

		int  priority() const override { return 6; }

		bool initialize() override { return true; }

		bool step(float time) override {
			scene->update();
			return true;
		}
		
		bool finalize() override { return true; }
	};
}

#endif
