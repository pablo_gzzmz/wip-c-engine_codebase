
#include "Window.hpp"

namespace engine_core {

	unordered_map<string, shared_ptr<Window>> Window::window_cache;

	// CONSTRUCTOR
	Window::Window(const string & _title, int width, int height, int posx, int posy, Uint32 FLAG) {
		title = _title;

		window = SDL_CreateWindow(title.c_str(), posx, posy, width, height, FLAG);

		// Bugs on creation without specific position set
		SDL_SetWindowPosition(window, posx, posy);

		if(window == NULL ){
			// Display error message
			printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
		}

		gl_context = SDL_GL_CreateContext(window);

		if (gl_context == NULL){
			// Display error message
			printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
		}

		glewInit();
	}


	Window * Window::create_window(bool & success, const string & _title, int width, int height, Uint32 FLAG, int posx, int posy) {
		if (window_cache.count(_title)) { success = false;  return nullptr; }

		shared_ptr<Window> new_window(new Window(_title, width, height, posx, posy, FLAG | SDL_WINDOW_SHOWN));

		window_cache.insert(std::make_pair(_title, new_window));

		success = true;

		return new_window.get();
	}
}