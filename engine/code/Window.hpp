
#ifndef ENGINE_WINDOW_HEADER
#define ENGINE_WINDOW_HEADER

#include <SDL.h>
#include <GL/glew.h>
#include <string>
#include <unordered_map>
#include <memory>

namespace engine_core {

	using std::string;
	using std::unordered_map;
	using std::shared_ptr;

	// Based on SDL 2.0
	class Window {
	public:
		enum Fullscreen_Type {
			REAL	= SDL_WINDOW_FULLSCREEN,
			DESKTOP = SDL_WINDOW_FULLSCREEN_DESKTOP
		};

	private:
		static unordered_map<string, shared_ptr<Window>> window_cache;

		SDL_Window    * window;
		SDL_GLContext   gl_context;

		string title;

	private:
		Window(const string & _title, int width, int height, int posx, int posy, Uint32 FLAG);

	public:
		~Window() {
			SDL_DestroyWindow	 (window);
			SDL_GL_DeleteContext (gl_context);
		}

	public:
		static Window * create_window (bool & success, const string & _title, int width, int height,
									   Uint32 FLAG = SDL_WINDOW_RESIZABLE,
									   int posx = 800, int posy = 60);

		static bool destroy_window(string _title) {
			if (window_cache.count(_title) > 0) {
				SDL_DestroyWindow(window_cache.at(_title)->get_window());
				return true;
			}
			return false;
		}

		static void destroy_all_windows() {
			for (auto it = window_cache.begin(); it != window_cache.end(); ++it) {
				SDL_DestroyWindow(it->second->get_window());
			}
		}

	public:
		static Window * get_window (string & _title) { return window_cache[_title].get();  }

		SDL_Window * get_window() { return window; }

		string		 get_title () { return title; }

		void set_fullscreen(Fullscreen_Type type = DESKTOP) {
			SDL_SetWindowFullscreen(window, type);
		}

		void set_position(int x, int y) {
			SDL_SetWindowPosition(window, x, y);
		}

		void set_brightness(float brightness) {
			SDL_SetWindowBrightness(window, brightness);
		}
		
		void set_vertical_sync() {
			SDL_GL_SetSwapInterval(-1);
		}

		void swap_buffer() {
			SDL_GL_SwapWindow(window);
		}
	};
}

#endif
