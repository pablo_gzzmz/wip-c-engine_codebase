
#include <SDL.h>
#undef   main

#include <cassert>
#include <string>
#include <memory>

#include "Scene.hpp"
#include "Kernel.hpp"
#include "Window.hpp"
#include "Event_Handler_Module.hpp"
#include "Input_Module.hpp"
#include "Messaging_Bus.hpp"
#include "Transform_Component.hpp"
#include "Entity.hpp"
#include "Update_Task.hpp"

#include "Render_Module.hpp"
#include "Render_Component.hpp"
#include "Camera_Component.hpp"
#include "Shader_Program.hpp"
#include "Model.hpp"

using namespace engine_core;
using namespace rendering;
using std::string;
using std::shared_ptr;

typedef Render_Module::Camera_Component Camera;
typedef Render_Module::Render_Component Renderer;
typedef Render_Module::Shader_Program	Shader_Program;

namespace engine_core {
#pragma region Testing classes
	class Mover : public Entity, public Listener{
	private:
		Entity * camera;

		float speed = 40;

		enum {
			STOPPED,
			FORWARD,
			BACKWARDS
		}move_state;

		enum {
			NONE,
			LEFT,
			RIGHT
		}rotation_state;

	public:
		Mover(string _name, Scene * _parent_scene, bool add_to_scene = true) : Entity(_name, _parent_scene, add_to_scene){}

		void set_camera(Entity * _camera) { camera = _camera; }

		void update() override {
			Vector3 v;

			switch (move_state) {
			case FORWARD:
				v = camera->get_transform()->forward();
				v.y = 0;
				v.z *= -1;
				transform->translate(v * speed * parent_scene->delta_time());
				break;

			case BACKWARDS:
				v = camera->get_transform()->forward();
				v.y = 0;
				v.z *= -1;
				transform->translate(-transform->forward() * speed * parent_scene->delta_time());
				break;
			}

			switch (rotation_state) {
			case LEFT:
				get_transform()->rotate_around_y(-0.05f);
				break;

			case RIGHT:
				get_transform()->rotate_around_y(0.05f);
				break;
			}
		}

		void handle_message(const Message & message) override {
			if (message.id == "key_down") {
				for (auto it = message.parameters.begin(); it != message.parameters.end(); it++) {
					if (it->first == "W"){
						move_state = FORWARD;
					}
					else if (it->first == "S") {
						move_state = BACKWARDS;
					}
					
					if (it->first == "A") {
						rotation_state = LEFT;
					}
					else if (it->first == "D") {
						rotation_state = RIGHT;
					}
				}
			}
			else if (message.id == "key_up") {
				for (auto it = message.parameters.begin(); it != message.parameters.end(); it++) {
					if (it->first == "W"){
						if (move_state == FORWARD) move_state = STOPPED;
					}
					else if (it->first == "S") {
						if (move_state == BACKWARDS) move_state = STOPPED;
					}

					if (it->first == "A") {
						if (rotation_state == LEFT) rotation_state = NONE;
					}
					else if (it->first == "D") {
						if (rotation_state == RIGHT) rotation_state = NONE;
					}
				}
			}
		}
	};

	class Camera_Controller : public Listener {
	private:
		Entity * target;
		Entity * camera;

	public:
		Camera_Controller(Entity * _target, Entity * _camera) : target(_target), camera(_camera){}
	};
}
#pragma endregion

int main() {

	// SDL INITIALIZATION
	if (SDL_Init(SDL_INIT_EVERYTHING) > 0) {
		// Display error message
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return 0;
	}

	bool success_checker = true;
	engine_core::Window * game_window = engine_core::Window::create_window(success_checker, "Game window", 800, 600, SDL_WINDOW_OPENGL);
	assert(success_checker);
	game_window->set_vertical_sync();


	Kernel kernel;

	Scene  scene_main ("main", &kernel);
	scene_main.set_window(game_window);

	// Update task (no module)
	Update_Task update_task = Update_Task(&scene_main);
	kernel.add_task(&update_task);

	// Event handling
	Event_Handler_Module event_handler_module("event_handler", &scene_main);

	// Input
	Input_Module input_module("input", &scene_main);

	// Rendering
	Render_Module render_module("render", &scene_main);

	// Camera object
	Entity camera_pivot = Entity("camera_pivot", &scene_main);
	Entity camera = Entity("camera", &scene_main, false);
	camera_pivot.add_child(&camera);
	Camera * cam = camera.add_component<Camera>();
	cam->set(50.f, 1.f, 200.f, GLfloat(800) / 600);
	camera_pivot.get_transform()->translate(Vector3(0, 12.f, 0));
	camera_pivot.get_transform()->rotate_around_y(3.14f);
	camera.get_transform()->rotate_around_x(0.52f);

	Mover cube = Mover("cube", &scene_main);
	Renderer * renderer = cube.add_component<Renderer>();
	renderer->load_model("..\\..\\assets\\arrow.obj");
	cube.get_transform()->translate(Vector3(0, 0, 25.f));
	cube.get_transform()->set_scale(0.1f);

	shared_ptr<Shader_Program> program(new Shader_Program("Default shader"));
	renderer->set_material(program.get());

	cube.set_camera(&camera);

	scene_main.get_messaging_bus()->register_listener("key_down", &cube);
	scene_main.get_messaging_bus()->register_listener("key_up",	  &cube);

	kernel.execute();

	SDL_Quit();

	return (EXIT_SUCCESS);
}